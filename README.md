#  Habits (iOS)

> Habits est un projet destiné à faciliter et rendre ludique le changement de certaines habitudes ou la réussite de nouveaux objectifs (ex: arrêter de fumer, faire plus de sport)

## Installation

1. Installer les dépendances du projet.
```
pod install
```

2. Ouvrir le fichier `.xcworkspace` avec Xcode.

L'API utilisée pour ce projet est déjà mise à disposition sur un serveur.

## Fonctionnalités

- [x] Création d'un compte
- [x] Connexion / Déconnexion
- [x] Connexion / Déconnexion via Facebook
- [x] Visualisation des objectifs
- [x] Ajout d'objectif
- [x] Mise à jour d'objectif

## Librairies annexes utilisées

- [Alamofire](https://github.com/Alamofire/Alamofire)
- [FacebookCore & FacebookLogin](https://github.com/facebook/facebook-sdk-swift)
- [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON)
- [Floaty](https://github.com/kciter/Floaty)
- [Toast-Swift](https://github.com/scalessec/Toast-Swift)
- [SideMenu](https://github.com/jonkykong/SideMenu)

## Meta

Développé par Arnaud BOUILLOD, Jessy HAVARD et Ronan HENRY

Cursus: 4ème année - IMIE Rennes



