import Toast_Swift
import FBSDKLoginKit
import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var myObjectivesView: UIView!
    @IBOutlet weak var myGroupsView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var disconnectView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        // hide navigation bar
        self.navigationController?.isNavigationBarHidden = true
        
        let preferences = UserDefaults.standard
        
        // set name and email values in menu
        if preferences.object(forKey: "firstname") != nil {
            setNameAndEmail(preferences: preferences)
        }
        
        // set FB profile picture
        if preferences.object(forKey: "picture_url") != nil {
            setFBPicture(preferences: preferences)
        }
    }
    
    func setNameAndEmail(preferences: UserDefaults) {
        let firstname = preferences.object(forKey: "firstname") as! String
        let lastname = preferences.object(forKey: "lastname") as! String
        let email = preferences.object(forKey: "email") as! String
        let textColor = Helper.hexStringToUIColor(hex: "#FEFEFE")
        
        self.nameLabel.text = firstname + " " + lastname
        self.nameLabel.textColor = textColor
        self.nameLabel.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        
        self.emailLabel.text = email
        self.emailLabel.textColor = textColor
    }
    
    func setFBPicture(preferences: UserDefaults) {
        let picture_url = preferences.object(forKey: "picture_url") as! String
        
        self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2
        self.imageView.clipsToBounds = true
        
        guard let url = URL(string: picture_url) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Failed fetching image:", error)
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                print("Not a proper HTTPURLResponse or statusCode")
                return
            }
            DispatchQueue.main.async {
                self.imageView.image = UIImage(data: data!)
            }
        }.resume()
    }
    
    override func viewDidLoad() {
        let myViews = [
            myObjectivesView,
            myGroupsView,
            profileView,
            disconnectView
        ]
        
        // add gesture recognizer on each view
        for uiView in myViews {
            uiView?.isUserInteractionEnabled = true
            uiView?.addGestureRecognizer(setGestureRecognizer())
        }
    }
    
    func setGestureRecognizer() -> UITapGestureRecognizer {
        return UITapGestureRecognizer(target: self, action: #selector(self.labelClicked))
    }
    
    @objc func labelClicked(sender:UITapGestureRecognizer) {
        switch sender.view?.tag {
        case 10:
            Helper.navigate(viewControllerIdentifier: "ObjectivesViewController", navigationController: self.navigationController!)
            break
        case 20:
            self.view.makeToast("Not yet implemented..", duration: 3.0)
            break
        case 30:
            Helper.navigate(viewControllerIdentifier: "ProfileViewController", navigationController: self.navigationController!)
            break
        case 40:
            self.disconnect()
            break
        default:
            break
        }
    }
    
    func disconnect() {
        // remove token
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "bearer_token") != nil {
            preferences.removeObject(forKey: "bearer_token")
        }
        
        if((FBSDKAccessToken.current()) != nil) {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
        }

        // redirect to login
        Helper.navigate(viewControllerIdentifier: "LoginViewController", navigationController: self.navigationController!)
    }
}
