import UIKit
import SwiftyJSON

class ObjectivesFormViewController: UIViewController, UIPickerViewDelegate , UIPickerViewDataSource {
 
    let categoryService = CategoryService()
    let objectiveService = ObjectiveService()
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var validateButton: UIButton!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    
    var pickerData: [String] = [String]()
    var currentEndDate: Date = Date()
    var currentStartDate: Date = Date()
    var currentCategory: String = ""
    var indexCategories: [String: String] = [:]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.currentCategory = pickerData[row]
        return pickerData[row]
    }
    
    override func viewDidLoad() {
        // get categories
        setCategories()

        self.picker.setValue(UIColor.white, forKey: "textColor")

        self.endDatePicker.timeZone = NSTimeZone.local
        self.startDatePicker.timeZone = NSTimeZone.local

        self.endDatePicker.setValue(UIColor.white, forKey: "textColor")
        self.startDatePicker.setValue(UIColor.white, forKey: "textColor")

        let preferences = UserDefaults.standard
        if preferences.object(forKey: "objective_id") != nil {
            //let id = preferences.integer(forKey: "objective_id")
            let objective_name = preferences.string(forKey: "objective_name")
            let end_date = preferences.string(forKey: "objective_endDate")
            let start_date = preferences.string(forKey: "objective_startDate")
            nameTextField.text = objective_name
            
            self.startDatePicker.setDate(ISO8601DateFormatter().date(from:start_date!)!, animated: true)
            self.endDatePicker.setDate(ISO8601DateFormatter().date(from:end_date!)!, animated: true)
        }
        
        validateButton.addTarget(self, action: #selector(validate), for: .touchUpInside)
        
        self.startDatePicker.addTarget(self, action: #selector(startDatePickerChanged(sender:)), for: .valueChanged)
        self.endDatePicker.addTarget(self, action: #selector(endDatePickerChanged(sender:)), for: .valueChanged)
    }
    
    @objc func endDatePickerChanged(sender:UIDatePicker) {
        self.currentEndDate = sender.date
    }
    
    @objc func startDatePickerChanged(sender:UIDatePicker) {
        self.currentStartDate = sender.date
    }
    
    @objc func validate() {
        let preferences = UserDefaults.standard
        var parameters: [String: AnyObject?] = [
            "name": nameTextField.text as AnyObject,
            "start_date": ISO8601DateFormatter().string(from: self.currentStartDate) as AnyObject,
            "end_date": ISO8601DateFormatter().string(from: self.currentEndDate) as AnyObject
        ]
       
        // Update
        if preferences.object(forKey: "objective_id") != nil {
            let id = preferences.integer(forKey: "objective_id")
            
            updateObjective(objectiveId: id, parameters: parameters)
        }
        else { // Create
            if currentCategory != "" {
                let categoryId = ["id": indexCategories[currentCategory]]
                parameters["categories"] = ["0": categoryId] as! [String: [String: String]] as AnyObject
                parameters["status"] = true as AnyObject
                
                createObjective(parameters: parameters)
            }
        }
    }
    
    func setCategories() {
        categoryService.getCategories(
            onSuccess: { items in
                for item:JSON in items as! [JSON] {
                    self.pickerData.append(item["name"].stringValue)
                    self.indexCategories[item["name"].stringValue] = item["id"].stringValue
                }
                self.picker.delegate = self as UIPickerViewDelegate
                self.picker.dataSource = self as UIPickerViewDataSource
            },
            onError: { error in
                print(error)
            }
        )
    }
    
    func createObjective(parameters: [String: AnyObject?]) {
        objectiveService.create(
            parameters: parameters,
            onSuccess: { response in
                self.navigateToObjectivesView()
            },
            onError: { error in
                print(error)
            }
        )
    }
    
    func updateObjective(objectiveId: Int, parameters: [String: AnyObject?]) {
        objectiveService.update(
            objectiveId: objectiveId,
            parameters: parameters,
            onSuccess: { response in
                self.navigateToObjectivesView()
            },
            onError: { error in
                print(error)
            }
        )
    }

    func navigateToObjectivesView() {
        Helper.navigate(viewControllerIdentifier: "ObjectivesViewController", navigationController: self.navigationController!)
    }
}
