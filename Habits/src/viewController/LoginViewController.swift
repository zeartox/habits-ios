import UIKit
import Alamofire
import FacebookLogin
import FBSDKLoginKit
import Toast_Swift

class LoginViewController: UIViewController {
    
    let authenticationService = AuthenticationService()
    
    var dict : [String : AnyObject]!
    
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var loginButtton: UIButton!
    @IBOutlet weak var facebookLogin: UIButton!
    @IBOutlet weak var twitterLogin: UIButton!
    @IBOutlet weak var googlePlusLogin: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        if (FBSDKAccessToken.current()) != nil{
            getFBUserData()
        }
        
        loginButtton.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
        facebookLogin.addTarget(self, action: #selector(self.facebookLoginClicked), for: .touchUpInside)
        twitterLogin.addTarget(self, action: #selector(self.notImplemented), for: .touchUpInside)
        googlePlusLogin.addTarget(self, action: #selector(self.notImplemented), for: .touchUpInside)
    }
    
    @objc func facebookLoginClicked() {
        let loginManager = LoginManager()
        
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                self.getFBUserData()
                print("Logged in!")
            }
        }
    }
    
    @objc func loginButtonClicked() {
        let email = emailTxtField.text as! String
        let password = passwordTxtField.text as! String
        
        if !Helper.validEmail(enteredEmail: email) {
            self.view.makeToast("Invalid email address", duration: 10.0)
            return
        }
        
        authenticate(email: email, password: password, social_connect: false, name: "")
    }
    
    @objc func notImplemented() {
        self.view.makeToast("Not yet implemented..", duration: 10.0)
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    
                    let email = self.dict["email"] as! String
                    let name = self.dict["name"] as! String
                    let picture_dict = self.dict["picture"]!["data"] as! [String : AnyObject]

                    let preferences = UserDefaults.standard
                    preferences.set(picture_dict["url"] as! String, forKey: "picture_url")
                    
                    self.authenticate(email: email, password: "", social_connect: true, name: name)
                }
            })
        }
    }
    
    func authenticate(email: String, password: String, social_connect: Bool, name: String) {
        authenticationService.login(
            email: email,
            password: password,
            social_connect: social_connect,
            onSuccess: { response in
                Helper.navigate(viewControllerIdentifier: "ObjectivesViewController", navigationController: self.navigationController!)
            },
            onError: { error in
                self.view.makeToast("Something wrong happened while trying to log in..", duration: 10.0)
                if social_connect {
                    self.register(email: email, password: password, name: name)
                }
            }
        )
    }

    func register(email: String, password: String, name: String) {
        var fullNameArr = name.split(separator: " ")
        
        let firstName: String = String(fullNameArr[0])
        let lastName: String = String(fullNameArr[1])
        let password = "A#" + String(Int(arc4random_uniform(999999) + 1))
        let parameters: [String: String] = ["email": email, "password": password, "first_name": firstName, "last_name": lastName]
        
        authenticationService.register(
            parameters: parameters,
            onSuccess: { response in
                self.authenticate(email: email, password: password, social_connect: true, name: name)
            },
            onError: { error in
                self.view.makeToast("Something wrong happened while trying to create your account..", duration: 10.0)
            }
        )
    }
}
