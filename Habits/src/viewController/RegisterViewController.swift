import Toast_Swift
import UIKit

class RegisterViewController: UIViewController {
    
    let authenticationService = AuthenticationService()
    
    @IBOutlet weak var firstnameTxtField: UITextField!
    @IBOutlet weak var lastnameTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var confirmPasswordTxtField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        signUpButton.addTarget(self, action: #selector(self.signUpButtonClicked), for: .touchUpInside)
    }
    
    @objc func signUpButtonClicked() {
        let firstname = firstnameTxtField.text as! String
        let lastname = lastnameTxtField.text as! String
        let email = emailTxtField.text as! String
        let password = passwordTxtField.text as! String
        let confirmPassword = confirmPasswordTxtField.text as! String
        
        let checkFields = validateFields(firstname: firstname, lastname: lastname, email: email, password: password, confirmPassword: confirmPassword)
        if checkFields["valid"] as! Bool == false {
            self.view.makeToast(checkFields["message"] as? String, duration: 10.0)
            return
        }
        
        signUp(firstname: firstname, lastname: lastname, email: email, password: password)
    }
    
    func validateFields(firstname: String, lastname: String, email: String, password: String, confirmPassword: String) -> [String: Any] {
        var res = [String: Any]()
        res["valid"] = false
        
        if !validLength(firstname: firstname, lastname: lastname, email: email, password: password, confirmPassword: confirmPassword) {
            res["message"] = "All the fields are required (firstname and lastname must be at least 2 characters long)"
            return res
        }
        
        if !Helper.validEmail(enteredEmail: email) {
            res["message"] = "Invalid email address"
            return res
        }
        
        if !validPassword(password: password, confirmPassword: confirmPassword) {
            res["message"] = "Invalid password or bad confirmation (password must be more than 7 characters, with at least one capital, numeric and special character)"
            return res
        }
        
        res["valid"] = true
        return res
    }
    
    func validLength(firstname: String, lastname: String, email: String, password: String, confirmPassword: String) -> Bool {
        return
            !firstname.isEmpty && firstname.count >= 2 &&
            !lastname.isEmpty && lastname.count >= 2 &&
            !email.isEmpty &&
            !password.isEmpty &&
            !confirmPassword.isEmpty
    }
    
    func validPassword(password: String, confirmPassword: String) -> Bool {
        let passwordFormat = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#;$%^&+=])[0-9A-Za-z@#;$%^&+=]{8,30}$"
        let passwordPredicate = NSPredicate(format:"SELF MATCHES %@", passwordFormat)
        return passwordPredicate.evaluate(with: password) && password == confirmPassword
    }
    
    func signUp(firstname: String, lastname: String, email: String, password: String) {
        let parameters: [String: String] = [
            "first_name": firstname,
            "last_name": lastname,
            "email": email,
            "password": password
        ]
        
        authenticationService.register(
            parameters: parameters,
            onSuccess: { response in
                self.navigationController?.popViewController(animated: true)
            },
            onError: { error in
                self.view.makeToast("Something wrong happened while trying to create your account..", duration: 10.0)
            }
        )
    }
}
