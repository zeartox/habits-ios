import Toast_Swift
import UIKit

class ResetPasswordViewController: UIViewController {
    
    let authenticationService = AuthenticationService()
    
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var resetButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        resetButton.addTarget(self, action: #selector(self.resetButtonClicked), for: .touchUpInside)
    }
    
    @objc func resetButtonClicked() {
        let email = emailTxtField.text as! String
        
        if !Helper.validEmail(enteredEmail: email) {
            self.view.makeToast("Invalid email address", duration: 10.0)
            return
        }
        
        self.view.makeToast("Not yet implemented.. (Not supported by our API for the moment)", duration: 10.0)
    }
}
