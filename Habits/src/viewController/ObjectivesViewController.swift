import UIKit
import SideMenu
import SwiftyJSON
import Floaty

class ObjectivesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FloatyDelegate {
    
    var objectiveList: [Objective] = []
    let objectiveService = ObjectiveService()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectiveList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let textLabel = cell.viewWithTag(1) as! UILabel
        let imageView = cell.viewWithTag(2) as! UIImageView
        
        imageView.image = UIImage(named: objectiveList[indexPath.row].categories[0].name.lowercased())
        textLabel.text = objectiveList[indexPath.row].name
        
        let uiView = cell.viewWithTag(3) as! UIView
        uiView.layer.cornerRadius = 0
        uiView.layer.shadowColor = UIColor.black.cgColor
        uiView.layer.shadowOpacity = 0.2
        uiView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Convert date to string
        let endDate = ISO8601DateFormatter().string(from:objectiveList[indexPath.row].endDate)
        let startDate = ISO8601DateFormatter().string(from:objectiveList[indexPath.row].startDate)
      
        let preferences = UserDefaults.standard
        preferences.set(objectiveList[indexPath.row].id, forKey: "objective_id")
        preferences.set(objectiveList[indexPath.row].name, forKey: "objective_name")
        preferences.set(endDate, forKey: "objective_endDate")
        preferences.set(startDate, forKey: "objective_startDate")
        preferences.synchronize()

        navigateToFormView()
    }
    
    func navigateToFormView() {
        Helper.navigate(viewControllerIdentifier: "ObjectivesFormViewController", navigationController: self.navigationController!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        
        let preferences = UserDefaults.standard
        
        if preferences.object(forKey: "id") != nil {
            let id = preferences.integer(forKey: "id")
            
            setObjectives(userId: id)
            setFloatingButton()
        }
        
        // define the menu
        setMenu()
    }
    
    func setFloatingButton() {
        let floaty = Floaty()
        floaty.buttonColor = Helper.hexStringToUIColor(hex: "#50C9CE")
        floaty.fabDelegate = self
        
        self.view.addSubview(floaty)
    }
    
    // on click on floating button
    func emptyFloatySelected(_ floaty: Floaty) {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "objective_id") != nil {
            preferences.removeObject(forKey: "objective_id")
        }
        self.navigateToFormView()
    }
    
    func setObjectives(userId: Int) {
        objectiveService.getObjectivesByUser(
            userId: userId,
            onSuccess: { items in
                for item:JSON in items as! [JSON] {
                    self.objectiveList.append(Objective(objective: item))
                }
                if self.objectiveList.count > 0 {
                    self.tableView.reloadData()
                }
            },
            onError: { error in
                print(error)
            }
        )
    }
    
    func setMenu() {
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "SideMenuNavigationController") as? UISideMenuNavigationController
        
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        
        menuButton.addTarget(self, action: #selector(self.showMenu), for: .touchUpInside)
    }
    
    @objc func showMenu() {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
}
