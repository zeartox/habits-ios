import UIKit
import SideMenu
import SwiftyJSON

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        let preferences = UserDefaults.standard
        
        setNameLabel(preferences: preferences)
        setMenu()
    }
    
    func setNameLabel(preferences: UserDefaults) {
        let firstname = preferences.object(forKey: "firstname") as! String
        let lastname = preferences.object(forKey: "lastname") as! String
        let textColor = Helper.hexStringToUIColor(hex: "#505050")
        
        self.nameLabel.text = firstname + " " + lastname
        self.nameLabel.textColor = textColor
    }
    
    func setMenu() {
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "SideMenuNavigationController") as? UISideMenuNavigationController
        
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        
        menuButton.addTarget(self, action: #selector(self.showMenu), for: .touchUpInside)
    }
    
    @objc func showMenu() {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
}
