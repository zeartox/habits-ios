import Alamofire
import Foundation

class RequestService {

    static let API_URL = "http://51.75.16.16/app.php/fr/api/"
    
    /* IMPORTANT:
     * parameters must be optional, do not send empty parameters to the API or it will be very slow to respond
     */
    func formatUrlRequest(url: String, method: String, parameters: [String: AnyObject?]?) throws -> URLRequest {
        // format url request
        let formattedUrl = try (RequestService.API_URL + url).asURL()
        
        var urlRequest = URLRequest(url: formattedUrl)
        
        // set method and header parameters
        urlRequest.httpMethod = method
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "bearer_token") != nil {
            let token = "Bearer " + preferences.string(forKey: "bearer_token")!
            urlRequest.addValue(token, forHTTPHeaderField: "X-AUTH-TOKEN")
        }
        
        // encode data
        return try JSONEncoding.default.encode(urlRequest, with: parameters)
    }
    
    func request(url: String, method: String, parameters: [String: AnyObject?]?, onSuccess: @escaping (Dictionary<String, Any>)->(), onError: @escaping (Dictionary<String, Any>)->()) {
        do {
            let request = try formatUrlRequest(url: url, method: method, parameters: parameters)
           
            Alamofire
                .request(request)
                .responseJSON { response in
                    
                    var result = Dictionary<String, Any>()
                    
                    if let statusCode = response.response?.statusCode {
                        result["code"] = statusCode
                        
                        switch statusCode {
                        case 200...202:
                            if let jsonResponse = response.result.value as? NSDictionary {
                                result["message"] = jsonResponse
                            }
                            onSuccess(result)
                            break
                        case 400...500:
                            if let jsonResponse = response.result.value as? NSDictionary {
                                if let errorValue = jsonResponse.value(forKey: "error") as? NSDictionary {
                                    result["message"] = errorValue.value(forKey: "message") as! String
                                }
                                onError(result)
                            }
                            break
                        default:
                            result["message"] = "Something wrong happened.."
                            onError(result)
                            break
                        }
                    }
            }
            
        } catch {
            print(error)
        }
    }
    
    /* To Guillaume:
     *
     * We did not achieve to set the request's method (from Alamofire) more generically..
     * So, we created these 4 next functions, which are really really repetitive and should be refactored in a certain way
     */
    func get(url: String, parameters: [String: AnyObject?]?, onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        self.request(
            url: url,
            method: HTTPMethod.get.rawValue,
            parameters: parameters,
            onSuccess: { response in onSuccess(response)},
            onError: { error in onError(error)}
        )
    }
    
    func post(url: String, parameters: [String: AnyObject?]?, onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        self.request(
            url: url,
            method: HTTPMethod.post.rawValue,
            parameters: parameters,
            onSuccess: { response in onSuccess(response)},
            onError: { error in onError(error)}
        )
    }

    func patch(url: String, parameters: [String: AnyObject?]?, onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        self.request(
            url: url,
            method: HTTPMethod.patch.rawValue,
            parameters: parameters,
            onSuccess: { response in onSuccess(response)},
            onError: { error in onError(error)}
        )
    }

    func delete(url: String, parameters: [String: AnyObject?]?, onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        self.request(
            url: url,
            method: HTTPMethod.delete.rawValue,
            parameters: parameters,
            onSuccess: { response in onSuccess(response)},
            onError: { error in onError(error)}
        )
    }
}
