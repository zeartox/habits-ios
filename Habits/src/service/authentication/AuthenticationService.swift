import Foundation

class AuthenticationService {
    
    static let BASE_URL = "users"
    
    let requestService = RequestService()
    
    func login(email: String, password: String, social_connect: Bool, onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        var parameters: [String: String] = [
            "email": email
        ]
        if social_connect {
            parameters["socialNetwork"] = "1"
        }
        else {
            parameters["password"] = password
        }
        
        // token should not be taken into account for the login
        removeToken()
    
        requestService.post(
            url: AuthenticationService.BASE_URL + "/login",
            parameters: parameters as [String : AnyObject?],
            onSuccess: { response in
                let message = (response as AnyObject)["message"]
                let user = (message as AnyObject)["user"]
                
                // Set prefecences
                let preferences = UserDefaults.standard
                preferences.set((user as AnyObject)["id"] as! Int, forKey: "id")
                preferences.set((message as AnyObject)["token"] as! String, forKey: "bearer_token")
                preferences.set((user as AnyObject)["email"] as! String, forKey: "email")
                preferences.set((user as AnyObject)["first_name"] as! String, forKey: "firstname")
                preferences.set((user as AnyObject)["last_name"] as! String, forKey: "lastname")
                
                //  Save to disk
                let didSave = preferences.synchronize()
                if !didSave {
                    //  Couldn't save
                }
                
                onSuccess(response)
            },
            onError: { error in
                onError(error)
            }
        )
    }
    
    func register(parameters: [String: String], onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        
        // token should not be taken into account if the user try to register
        removeToken()
        
        requestService.post(
            url: AuthenticationService.BASE_URL + "/register",
            parameters: parameters as [String : AnyObject?],
            onSuccess: { response in
                let message = (response as AnyObject)["message"]
                let user = (message as AnyObject)["user"]
                
                onSuccess(response)
            },
            onError: { error in
                onError(error)
            }
        )
    }
    
    private func removeToken() {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "bearer_token") != nil {
            preferences.removeObject(forKey: "bearer_token")
        }
    }
}
