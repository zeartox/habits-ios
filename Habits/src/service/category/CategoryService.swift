import Alamofire
import Foundation
import SwiftyJSON

class CategoryService {
    
    static let BASE_URL = "categories"
    
    let requestService = RequestService()
    
    func getCategories(onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        requestService.get(
            url: CategoryService.BASE_URL,
            parameters: nil,
            onSuccess: { response in
                print(response)
                
                let jsonResponse = JSON(response)
                let content = jsonResponse["message"]
                let items: [JSON] = content["items"].array!
                
                onSuccess(items)
            },
            onError: { error in
                onError(error)
            }
        )
    }
}
