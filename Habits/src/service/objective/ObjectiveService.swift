import Alamofire
import Foundation
import SwiftyJSON

class ObjectiveService {
    
    static let BASE_URL = "objectives"
    
    let requestService = RequestService()
    
    func getObjectivesByUser(userId: Int, onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        requestService.get(
            url: ObjectiveService.BASE_URL + "/users/" + String(userId),
            parameters: nil,
            onSuccess: { response in
                print(response)
                
                let jsonResponse = JSON(response)
                let content = jsonResponse["message"]
                let items: [JSON] = content["items"].array!
                
                onSuccess(items)
            },
            onError: { error in
                onError(error)
            }
        )
    }
    
    func update(objectiveId: Int, parameters: [String: AnyObject?], onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        requestService.patch(
            url: ObjectiveService.BASE_URL + "/" + String(objectiveId),
            parameters: parameters,
            onSuccess: { response in
                print(response)
                
                // TODO
                
                onSuccess(response)
            },
            onError: { error in
                onError(error)
            }
        )
    }
    
    func create(parameters: [String: AnyObject?], onSuccess: @escaping (Any)->(), onError: @escaping (Any)->()) {
        requestService.post(
            url: ObjectiveService.BASE_URL + "/add",
            parameters: parameters,
            onSuccess: { response in
                //print(response)
                onSuccess(response)
            },
            onError: { error in
                onError(error)
            }
        )
    }
}
