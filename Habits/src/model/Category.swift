import Foundation
import SwiftyJSON

class Category {
    
    var id: Int
    var name: String
    
    init(category: JSON) {
        id = category["id"].intValue
        name = category["name"].stringValue
    }
}
