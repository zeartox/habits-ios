import Foundation
import SwiftyJSON

class User {
    
    var id: Int
    var firstName: String
    var lastName: String
    var email: String
    var password: String
    var isActive: Bool
    //var socialNetwork: Bool

    init(user: JSON) {
        id = user["id"].intValue
        firstName = user["first_name"].stringValue
        lastName = user["last_name"].stringValue
        email = user["email"].stringValue
        password = user["password"].stringValue // TODO: remove this ?
        isActive = user["isActive"].bool!
    }
    
}
