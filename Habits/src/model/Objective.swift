import Foundation
import SwiftyJSON

class Objective {
    
    var id: Int
    var name: String
    var created: Date
    var startDate: Date
    var endDate: Date
    var status: Bool
    var categories: [Category] = []
    
    init(objective: JSON) {
        id = objective["id"].intValue
        name = objective["name"].stringValue
        created = ISO8601DateFormatter().date(from:objective["created"].stringValue)!
        startDate = ISO8601DateFormatter().date(from:objective["start_date"].stringValue)!
        endDate = ISO8601DateFormatter().date(from:objective["end_date"].stringValue)!
        status = objective["status"].bool!
        
        let categoryList: [JSON] = objective["categories"].array!
        for category: JSON in categoryList {
            categories.append(Category(category: category))
        }
    }
    
}
